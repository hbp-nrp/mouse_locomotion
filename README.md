# MOUSE LOCOMOTION EXPERIMENT


## AUTHOR: Shravan Tata Ramalingasetty


## EMAIL: shravantr@gmail.com


## Goal :

The goal of this milestone is to demonstrate locomotor behavior of a
neuromechanical mouse model in the NeuroRoboticsPlatform.


# Installation


## Requirements

-   Python 2
-   Cython
-   pip


## Dependencies

-   [farms\_pylog](https://gitlab.com/farmsim/farms_pylog)
-   [farms\_container](https://gitlab.com/farmsim/farms_container)
-   [farms\_network](https://gitlab.com/farmsim/farms_network)


## Steps for local install


### Navigate to the local installation of the NRP in a terminal (same for docker installation as well)


### Activate the virtual environment of NRP

    source $VIRTUAL_ENV/bin/activate


### Installation of farms\_pylog

    pip install git+https://gitlab.com/farmsim/farms_pylog.git


### Installation of farms\_container

-   Since NRP is currently using python 2.7, we need to checkout to a specific version of farms\_container

        pip install git+https://gitlab.com/farmsim/farms_container.git@python-2.7#egg=farms_container


### Installation of farms\_network

-   Since NRP is currently using python 2.7, we need to checkout to a specific version of farms\_network

        pip install git+https://gitlab.com/farmsim/farms_network.git@python-2.7#egg=farms_network


# Model Description

The mouse skeletal model is obtained from a CT-scan. The skeletal model is fully rigged with the necessary degrees of freedom between any two links. The joint centers are anatomically relevant. In the current version all rotations are limited to simple revolute joints. If necessary, the user may define more complex joint rotations using the gazebo joints API in NRP. The model development is currently performed using the open source Blender software. The next aspect of the model is the computation of the physical properties of the links. The properties are computed based on simple bounding objects which enclose any given link with minimum area. The masses and inertias of each link are then computed with the assumption that each of the bounding boxes have a uniform distribution of water density. This is a fair assumption while considering the complete model. The total computed mass of the full model with this assumption (~25 g) is in accordance with the average mass of actual mice. The user is still free to update the mass of any link with more accurate data from actual measurements. Figure 1 shows the full model. For the purpose of locomotion experiment, the model template is configured to have a reduced set of degrees of freedom. The locomotion template model has only flexion-extension joints, four joints in each hind limb, two in the spine and three in the forelimb (the other DOFs are blocked in their default joint angle). In total, the model for locomotion template consists of sixteen joints. For the purpose of stability, the model is supported at the pelvis by planar and revolute joints so as to constrain the model to the sagittal plane. The user may disable these constraints to make the model free in all three dimensions. Figure 1 shows the full mouse model and the reduced template model used for locomotion experiment. The original source model can be found at [mouse\_model repository](https://gitlab.com/hbp-nrp/mouse_model.git)

![img](./figures/locomotion_model_side_view.png "Mouse hind limb sagital locomotion skeleton model")


# Muscle Description

The skeletal model is actuated by muscles to apply necessary forces to produce motion. Two different types of muscle models are supported as part of the locomotion template: users can choose between the Hill-type model (as initially planned) and additionally a simpler spring-damper muscle models. TheA simple spring-damper muscle model which is useful for active torque control in simple models and thea more detailed hill-muscle model for more complex scenarios. Integration of the NeuroRoboticsPlatform (NRP) with opensim allows for the direct use of standard Hill-muscle implementations. For the mouse hindlimb, there was some previous research data available for the attachment points and muscle data. Though this helped a lot, still substantial amounts of work had to be done to port the data to the mouse model in the NRP. Mainly the muscle attachment points had to be recomputed for the NRP model from the literature data. This was achieved by the process of mesh-registration between the hindlimb bones in literature and the NRP model. Each segment of the hindlimb appropriate landmarks were picked on both the NRP model and literature data. Mesh registration computes the transformation matrix between the chosen landmarks. The transformation matrix is then used to transfer the muscle attachment points from the literature data to the NRP model. Figure 2, shows the transferred muscle attachments of the hindlimb and visualized in Blender. Due to the lack of data about mouse forelimb muscles, Hill-type muscles could not be setup in the NRP.
For the purpose of demonstrating the locomotion experiment, simple spring-damper muscles were used. Each of the flexion-extension joint described in the skeletal model section is actuated by spring-damper muscles driven by a Central Pattern Generator (CPG) network. The spring-damper muscle model is provided as an external python library while the Hill-muscle models are available with the Opensim interface of NRP. The model described above was also used in different forms in several locomotion studies independently [1] [2] [3]
For more complex muscle models check out the [muscle library](https://gitlab.com/farmsim/farms_muscle.git)


# Network Description

The next step in the locomotion control experiment was to control/actuate the muscles. This was achieved using neural networks. A neural network library has been developed. The library has a high level python interface and the neuronal models themselves are developed in Cython for efficiency. The library currently contains ten different neuronal models, ranging from simple oscillators to hodgkin-huxley type neuron models. For the purpose of demonstration the neuronal model type used as Central Pattern Generators (CPG) are amplitude controlled phase oscillators with morphed sinusoid output signals. Figure 3 shows the example network setup in the locomotion template. The user has several options to extend the network complexity or even completely rewrite the network from scratch. Even though not used in the demonstrator, feedback signals from the physics simulators (joint positions, joint velocities, joint torques, ground reaction forces) and afferents from muscles(Ia, Ib and II) are available for the users to setup closed loop locomotion experiments.

![img](./figures/Network.png "Mouse hind limb sagital locomotion CPG network")


# References

1.  Cerebellar modulation of the central pattern generator in a simulated mouse T. P. Jensen, S. Tata, A. J. Ijspeert & S. Tolu : Neural Control of Movement 2020
2.  An integrated neurobiomechanical model of the mouse to study neural control of locomotion. Shravan Tata Ramalingasetty, Simon M. Danner, Auke J. Ijspeert, Ilya A. Rybak : AMAM  2019, DOI: 10.5075/epfl-BIOROB-AMAM2019-52
3.  Closed-Loop Neuro-Muscular Modelling of Murine for Locomotion Studies, Shravan Tata Ramalingasetty, Auke J. Ijspeert : World Congress of Biomechanics, 2017
