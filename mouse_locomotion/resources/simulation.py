import numpy as np
from farms_network.neural_system import NeuralSystem
from farms_container import Container
from utils import (
    read_optimization_results, sort_optimization_parameters,
    update_active_muscle_parameters, update_passive_muscle_parameters,
    update_network_phases, add_muscle_namespace_to_container,
    create_active_muscles, create_passive_muscles,
    add_joint_log_to_container, update_joints_log,
)
import farms_pylog as pylog


class Simulation(object):
    """Mouse locomotion simulation
    """

    def __init__(self, controller_config, logger=pylog):
        super(Simulation, self).__init__()
        self.logger = logger
        self.actuated_joints = [
            'LHip', 'LKnee', 'LAnkle',
            'RHip', 'RKnee', 'RAnkle',
            'LShoulder', 'LElbow', 'LWrist',
            'RShoulder', 'RElbow', 'RWrist',
        ]
        self.passive_joints = ['Thoracic']
        self.fixed_joints = [
            'Head', 'Cervical', 'Lumbar',
            'LMtp', 'RMtp',
            'LPalm', 'RPalm',
        ]
        self.joints = (
            self.actuated_joints + self.passive_joints + self.fixed_joints
        )
        #: Create container
        self.container = Container()
        self.logger.info("Container created...")
        #: Create neural system
        self.control = NeuralSystem(
            controller_config,
            self.container
        )
        self.logger.info("Neural system created...")
        self.noscillators = self.control.graph.number_of_nodes()
        #: Setup container for joints
        add_joint_log_to_container(self.container, self.joints)
        add_muscle_namespace_to_container(self.container)
        #: Muscles
        self.active_muscles = create_active_muscles(
            self.container, self.actuated_joints
        )
        self.passive_muscles = create_passive_muscles(
            self.container, self.passive_joints
        )
        self.logger.info("Muscles created...")
        #: initialize self.container
        self.container.initialize()
        #: setup controller integrator
        self.control.setup_integrator()
        self.logger.info("Container and controller initialized...")
        #: Read optimization parameters
        fun, var = read_optimization_results(
            "resources/FUN.74",
            "resources/VAR.74"
        )
        self.logger.info("Read optimization results...")
        params = var[np.argmin(fun[:, 0])]
        active, passive, phases = sort_optimization_parameters(
            self.noscillators, params
        )
        self.logger.info("Sorted parameters...")
        #: Update parameters
        update_active_muscle_parameters(
            self.actuated_joints, self.active_muscles, active
        )
        self.logger.info("Updated active muscle parameters...")
        update_passive_muscle_parameters(
            self.passive_joints, self.passive_muscles, passive
        )
        self.logger.info("Updated passive muscle parameters...")
        update_network_phases(self.container, phases)
        self.logger.info("Updated network phase parameters...")
        self.logger.info("Model initialization complete! ")

    def step(self, joint_states, time_step=1e-3):
        """ Step the simulation.
        """
        #: update the joints log in container
        update_joints_log(
            self.container, self.joints, joint_states, self.logger
        )
        #: step the neural network
        self.control.step(time_step)
        #: compute muscle states
        torques = {}
        for joint, muscle in self.active_muscles.items():
            torques[joint] = muscle.compute_torque(only_passive=False)
        for joint, muscle in self.passive_muscles.items():
            torques[joint] = muscle.compute_torque(only_passive=True)
        return torques


def main():
    """ Test Simulation """
    sim = Simulation('./quadruped_locomotion.graphml')


if __name__ == '__main__':
    main()
