import numpy as np
import farms_pylog as pylog
from spring_damper_muscle import Parameters
from spring_damper_muscle import SDAntagonistMuscle

########## CONTAINER ##########


def add_muscle_namespace_to_container(container):
    """ Initialize container with muscle log data.
    Parameters
    ----------
    container: <Container>
        Instance of farms container
    """
    container.add_namespace('muscle')
    container.muscle.add_table('parameters', table_type='CONSTANT')
    container.muscle.add_table('outputs')


def add_joint_log_to_container(container, joints):
    """ Initialize container with joint log data.
    Parameters
    ----------
    container: <Container>
        Instance of farms container
    joints: <list>
        List of joint names in the container
    """
    #: Add joints namespace to container
    joint_data = container.add_namespace('joints')
    #: Add Tables to joints container
    joint_data.add_table('positions')
    joint_data.add_table('velocities')
    #: Add parameters
    for joint in joints:
        joint_data.positions.add_parameter(joint)
        joint_data.velocities.add_parameter(joint)


def update_joints_log(container, joints, joint_states, clientLogger=None):
    """ Update container joint position and velocities.
    Parameters
    ----------
    container: <Container>
        Instance of farms container
    joints: <list>
        List of joint names to update the log for
    joint_states: <ros::sensor_msgs::msg::JointState>
        ROS topic of type JointState
    """
    for i in range(0, len(joint_states.value.name)):
        joint = joint_states.value.name[i]
        if joint in joints:
            pos = joint_states.value.position[i]
            vel = joint_states.value.velocity[i]
            if clientLogger:
                clientLogger.info(
                    "{} -> pos : {} vel : {}".format(joint, pos, vel)
                )
                container.joints.positions.get_parameter(
                    joint
                ).value = pos
                container.joints.velocities.get_parameter(
                    joint
                ).value = vel

########## MUSCLES ##########


def create_active_muscles(container, joints):
    """ Create active antagonist muscles for all the joints.
    Parameters
    ----------
    container: <Container>
        Instance of farms container
    joints: <list>
        List of joint names
    """
    muscles = {}
    for joint in joints:
        muscles[joint] = SDAntagonistMuscle(
            container,
            name=joint,
            joint_pos=container.joints.positions.get_parameter(joint),
            joint_vel=container.joints.velocities.get_parameter(joint),
            rest_pos=0.0,
            flexor_mn=container.neural.states.get_parameter(
                'phase_' + joint + '_flexion'
            ),
            extensor_mn=container.neural.states.get_parameter(
                'phase_' + joint + '_extension'
            ),
            flexor_amp=container.neural.states.get_parameter(
                'amp_' + joint + '_flexion'
            ),
            extensor_amp=container.neural.states.get_parameter(
                'amp_' + joint + '_extension'
            )
        )
    return muscles


def create_passive_muscles(container, joints):
    """ Create passive antagonist muscles for all the joints.
    Parameters
    ----------
    container: <Container>
        Instance of farms container
    joints: <list>
        List of joint names
    """
    muscles = {}
    for joint in joints:
        muscles[joint] = SDAntagonistMuscle(
            container,
            name=joint,
            joint_pos=container.joints.positions.get_parameter(joint),
            joint_vel=container.joints.velocities.get_parameter(joint),
            rest_pos=0.0,
            flexor_mn=None,
            extensor_mn=None,
            flexor_amp=None,
            extensor_amp=None
        )
    return muscles


def read_optimization_results(fun, var):
    """ Read optimization results.
    Parameters
    ----------
    fun: <str>
        Path to the optimization objectives results
    var: <str>
        Path to the optimization variables results

    """
    pylog.debug("Reading optimization results: {} and {}".format(fun, var))
    return (np.loadtxt(fun), np.loadtxt(var))


def sort_optimization_parameters(noscillators, params):
    """ Update simulation parameters """
    N = int(noscillators/4)

    active_muscle_gains = np.asarray(params[:7*N])
    passive_muscle_gains = np.asarray(params[7*N:4+(7*N)])
    phases = np.asarray(params[4+(7*N):])

    #: If log-scale
    indices = np.full(
        np.shape(active_muscle_gains), True, dtype=bool
    )
    indices[4::7] = False
    indices[5::7] = False
    indices[6::7] = False
    active_muscle_gains[indices] = 10**(
        active_muscle_gains[indices]
    )
    passive_muscle_gains = 10**(passive_muscle_gains)

    # #: check solutions
    assert not np.any(np.isnan(active_muscle_gains))
    assert not np.any(np.isnan(passive_muscle_gains))
    assert not np.any(np.isnan(phases))

    pylog.debug(
        "Opti active muscle gains {}".format(
            active_muscle_gains
        )
    )
    pylog.debug(
        "Opti passive muscle gains {}".format(
            passive_muscle_gains
        )
    )
    pylog.debug("Opti phases {}".format(phases))

    return active_muscle_gains, passive_muscle_gains, phases


def update_active_muscle_parameters(actuated_joints, active_muscles, params):
    """
    Update active muscle parameters
    Parameters
    ----------
    actuated_joints: <tuple>
        Tuple of actuated joints in the model
    active_muscles: <dict>
        Dictionary of SDAntagonistMuscle active muscles in the model
    params: <np.array>
        Active muscle parameters

    """
    #: update active muscle parameters
    symmetry_joints = filter(
        lambda x: x[0] != 'R', actuated_joints
    )
    for j, joint in enumerate(symmetry_joints):
        pylog.debug(
            '{} parameter -> {}'.format(joint, params[7*j:7*(j+1)])
        )
        active_muscles[joint].update_parameters(
            Parameters.from_array(params[7*j:7*(j+1)])
        )
        active_muscles[joint.replace('L', 'R', 1)].update_parameters(
            Parameters.from_array(params[7*j:7*(j+1)])
        )


def update_passive_muscle_parameters(
        passive_joints, passive_muscles, params
):
    """Update passive muscle parameters

    passive_joints: <tuple>
        Tuple of passive joints in the model
    passive_muscles: <dict>
        Dictionary of SDAntagonistMuscle passive muscles in the model
    params: <np.array>
        Passive muscle parameters
    """

    #: update passive muscle parameters
    for j, joint in enumerate(passive_joints):
        p = list(params[4*j:4*(j+1)]) + [0, 0, 0]
        passive_muscles[joint].update_parameters(
            Parameters.from_array(p)
        )


def update_network_phases(container, params):
    """ Update neural network phases.

    Parameters
    ----------
    container: <Container>
       Instance of farms data container
    params: <np.array>
        Phase parameters

    """

    nparameters = container.neural.parameters

    #: Update phases
    #: Edges to set phases for
    phase_edges = [
        ('Hip', 'Knee'),
        ('Knee', 'Ankle'),
        ('Shoulder', 'Elbow'),
        ('Elbow', 'Wrist')
    ]
    print(params)
    for side in ('L', 'R'):
        for j1, ed in enumerate(phase_edges):
            for j2, action in enumerate(('flexion', 'extension')):
                node_1 = "{}{}_{}".format(side, ed[0], action)
                node_2 = "{}{}_{}".format(side, ed[1], action)
                nparameters.get_parameter(
                    'phi_{}_to_{}'.format(node_1, node_2)
                ).value = params[2*j1 + j2]
                nparameters.get_parameter(
                    'phi_{}_to_{}'.format(node_2, node_1)
                ).value = -1*params[2*j1 + j2]
