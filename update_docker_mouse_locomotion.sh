#!/bin/bash

cd ~/tatarama/mouse_locomotion
echo "Changed directory to : " $PWD
git pull origin master
echo "Git pull successful!"
rm -r $NRP_EXPERIMENTS_DIRECTORY/mouse_locomotion
echo "Removed mouse locomotion experimetns folder!"
rm -r $NRP_MODELS_DIRECTORY/mouse_locomotion_model
echo "Removed mouse locomotion models folder!"
cp -r mouse_locomotion $NRP_EXPERIMENTS_DIRECTORY
echo "Copied mouse locomotion experiments folder!"
cp -r gazebo_models/mouse_locomotion_model $NRP_MODELS_DIRECTORY
echo "Copied mouse locomotion models folder!"
cd $NRP_MODELS_DIRECTORY
echo "Changed directory to : " $PWD
bash create-symlinks.sh
echo "Created models symlinks"
