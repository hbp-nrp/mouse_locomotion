/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
/*
 * Desc: Gazebo plugin providing generic controllers for robot joints
 * This plugin provides ROS topics to control single joints of the robot. The controlled joints can be specified in the SDF as plugin tags
 * Author: Lars Pfotzer
 */

#include "generic_joint_state_publisher_plugin.h"

#include <boost/bind.hpp>
#include <sensor_msgs/JointState.h>
#include <ros/time.h>

namespace gazebo
{

GenericJointStatePublisherPlugin::GenericJointStatePublisherPlugin()
{
  m_nh = ros::NodeHandle();
}

GenericJointStatePublisherPlugin::~GenericJointStatePublisherPlugin()
{
  m_nh.shutdown();
}

void GenericJointStatePublisherPlugin::Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
{
  // Store the pointer to the model
  m_model = parent;  
  m_joints = m_model->GetJoints();

  //sdf::ElementPtr sdf->GetElement("");
  ROS_INFO("sdf name %s, sdf description %s", sdf->GetName().c_str(), sdf->GetDescription().c_str());

  // Controller time control.
  this->lastControllerUpdateTime = this->m_model->GetWorld()->SimTime();

  int numJoints = m_joints.size();
  m_js.header.stamp.sec = this->lastControllerUpdateTime.sec;
  m_js.header.stamp.nsec = this->lastControllerUpdateTime.nsec;
  m_js.name.resize ( numJoints );
  m_js.position.resize ( numJoints );
  m_js.velocity.resize ( numJoints );
  m_js.effort.resize ( numJoints );

  // Listen to the update event. This event is broadcast every simulation iteration.
  m_updateConnection = event::Events::ConnectBeforePhysicsUpdate(boost::bind(&GenericJointStatePublisherPlugin::OnUpdate, this, _1));

  const std::string joint_states_topic_name = m_model->GetName() + "/" + "joint_states";

  this->m_joint_state_pub = m_nh.advertise<sensor_msgs::JointState>( joint_states_topic_name, 10 );

  m_ja.layout.dim.resize(1);
  m_ja.layout.dim[0].label = "accelerations";
  m_ja.layout.dim[0].size  = numJoints;
  m_ja.layout.dim[0].stride = 1;
  m_ja.data.resize(numJoints);

  const std::string joint_accel_topic_name = m_model->GetName() + "/" + "joint_accel";

  this->m_joint_accel_pub = m_nh.advertise<std_msgs::Float32MultiArray>( joint_accel_topic_name, 10 );
}

// Called by the world update start event
void GenericJointStatePublisherPlugin::OnUpdate(const common::UpdateInfo & /*_info*/)
{
  std::lock_guard<std::mutex> lock(this->mutex);

  gazebo::common::Time curTime = this->m_model->GetWorld()->SimTime();

  if (curTime > this->lastControllerUpdateTime)
  {
    m_js.header.stamp.sec = curTime.sec;
    m_js.header.stamp.nsec = curTime.nsec;
    // Update the control surfaces and publish the new state.
    int curr_ind = 0;
    for (physics::Joint_V::iterator joint_iter = m_joints.begin(); joint_iter != m_joints.end(); ++joint_iter, ++curr_ind)
    {
      physics::JointPtr joint = *joint_iter;
      m_js.name[curr_ind] = joint->GetName();
      m_js.position[curr_ind] = joint->Position();
      m_js.velocity[curr_ind] = joint->GetVelocity(0);
      m_js.effort[curr_ind] = joint->GetForce(0);
      m_ja.data[curr_ind] = joint->GetAcceleration(0);
    }

    m_joint_state_pub.publish ( m_js );
    m_joint_accel_pub.publish ( m_ja );
  }
  this->lastControllerUpdateTime = curTime;

}

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(GenericJointStatePublisherPlugin)

} // namespace gazebo
