/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
#ifndef GENERIC_JOINT_STATE_PUBLISHER_PLUGIN_H
#define GENERIC_JOINT_STATE_PUBLISHER_PLUGIN_H

#include <vector>
#include <string>
#include <gazebo/common/Events.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/gazebo.hh>

#include <gazebo/msgs/msgs.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/transport/Node.hh>
#include <gazebo/transport/Publisher.hh>

#include <std_msgs/Float64.h>
#include <std_msgs/Float32MultiArray.h>
#include <geometry_msgs/Vector3.h>
#include <sensor_msgs/JointState.h>

#include <ros/ros.h>

#include <mutex>

namespace gazebo
{

/* using namespace generic_joint_state_publisher_plugin; */

class GenericJointStatePublisherPlugin : public ModelPlugin
{

public:

  GenericJointStatePublisherPlugin();
  ~GenericJointStatePublisherPlugin();

  // Load the plugin and initilize all controllers
  void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);

  // Simulation update callback function
  void OnUpdate(const common::UpdateInfo & /*_info*/);

private:

  // ROS node handle
  ros::NodeHandle m_nh;

  // Pointer to the model
  physics::ModelPtr m_model;

  // Vector of joint pointers
  physics::Joint_V m_joints;

  // Pointer to the update event connection
  event::ConnectionPtr m_updateConnection;

  /// \brief keep track of controller update sim-time.
  private: gazebo::common::Time lastControllerUpdateTime;

  /// \brief Controller update mutex.
  private: std::mutex mutex;

  // ROS joint state publisher
  private: ros::Publisher m_joint_state_pub;
  private: sensor_msgs::JointState m_js;
  private: ros::Publisher m_joint_accel_pub;
  private: std_msgs::Float32MultiArray m_ja;
};

} // namespace gazebo

#endif
